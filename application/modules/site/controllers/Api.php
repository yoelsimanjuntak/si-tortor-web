<?php
class Api extends MY_Controller {
  public function __construct()
  {
    parent::__construct();
    header('Access-Control-Allow-Origin: *');
  }

  public function login() {
    $this->load->model('muser');
    $json = file_get_contents('php://input');
    $data = json_decode($json);

    if(empty($json)) {
      ShowJsonError('JSON tidak valid.');
      return;
    }

    $username = $this->input->post('username');
    $password = $this->input->post('password');

    if($this->muser->authenticate($username, $password)) {
      if($this->muser->IsSuspend($username)) {
        ShowJsonError('Akun anda di suspend.');
        return;
      }

      $this->db->where(COL_USERNAME, $username);
      $this->db->update(TBL__USERS, array(COL_LASTLOGIN=>date('Y-m-d H:i:s'), COL_LASTLOGINIP=>$this->input->ip_address()));

      $userdetails = $this->muser->getdetails($username);
      //$token = GetEncryption($userdetails[COL_USERNAME]);
      ShowJsonSuccess('Login berhasil.', array('data'=>array(
        'Email' => $userdetails[COL_EMAIL],
        'Role' => $userdetails[COL_ROLENAME],
        'ID_Role' => $userdetails[COL_ROLEID],
        'Nama' => $userdetails[COL_NAME],
        //'Token' => $token)));
      )));
    } else {
      ShowJsonError('Username / password tidak tepat.');
    }
  }

  public function changeprofile() {
    $this->load->model('muser');
    $username = $this->input->post('username');
    $name = $this->input->post('name');
    $res = $this->db->where(COL_USERNAME, $username)->update(TBL__USERINFORMATION, array(COL_NAME=>$name));
    if(!$res) {
      ShowJsonError('Terjadi kesalahan pada server.');
      exit();
    }

    $userdetails = $this->muser->getdetails($username);
    ShowJsonSuccess('Berhasil memperbarui profil', array('data'=>array(
      'Email' => $userdetails[COL_EMAIL],
      'Role' => $userdetails[COL_ROLENAME],
      'ID_Role' => $userdetails[COL_ROLEID],
      'Nama' => $userdetails[COL_NAME],
      //'Token' => $token)));
    )));
    exit();
  }

  public function projects() {
    $uname = $this->input->post(COL_USERNAME);
    $ruser = $this->db
    ->where(COL_USERNAME,$uname)
    ->get(TBL__USERS)
    ->row_array();
    if(empty($ruser)) {
      ShowJsonError('Autentikasi gagal.');
      exit();
    }

    if($ruser[COL_ROLEID]==ROLEPENGAWAS) {
      $this->db->Where("EXISTS(select mem.Username from tprojectmember mem where mem.PrID=tproject.Uniq and mem.UserName='$uname')");
    }
    $res = $this->db
    ->select('*, (select count(*) from twork work where work.PrID = tproject.Uniq and work.IsDeleted=0) as NumDetail')
    //->where("(CreatedBy='$uname' or EXISTS(select mem.Username from tprojectmember mem where mem.PrID=tproject.Uniq and mem.UserName='$uname'))")
    ->where(COL_ISDELETED, 0)
    ->order_by(COL_CREATEDON, 'desc')
    ->get(TBL_TPROJECT)
    ->result_array();
    ShowJsonSuccess('Loaded.', array('data'=>$res));
  }

  public function notification() {
    $uname = $this->input->post(COL_USERNAME);
    $ruser = $this->db
    ->where(COL_USERNAME,$uname)
    ->get(TBL__USERS)
    ->row_array();
    if(empty($ruser)) {
      ShowJsonError('Autentikasi gagal.');
      exit();
    }

    if($ruser[COL_ROLEID]==ROLEPENGAWAS) {
      $this->db->Where("EXISTS(select mem.Username from tprojectmember mem where mem.PrID=tproject.Uniq and mem.UserName='$uname')");
    }
    $rprojects = $this->db
    ->select('*, (select count(*) from twork work where work.PrID = tproject.Uniq and work.IsDeleted=0) as NumDetail')
    //->where("(CreatedBy='$uname' or EXISTS(select mem.Username from tprojectmember mem where mem.PrID=tproject.Uniq and mem.UserName='$uname'))")
    ->where(COL_ISDELETED, 0)
    ->order_by(COL_CREATEDON, 'desc')
    ->get(TBL_TPROJECT)
    ->result_array();

    $arrnotifs = array();
    foreach($rprojects as $p) {
      $qselect = @"
      twork.*,
      tproject.PrName,
      ((select sum(wd.WorkDetWeight) from tworkdetails wd where wd.WorkID=twork.Uniq and wd.WorkDetCategory='PONDASI' and wd.WorkDetIsComplete=1)/(select sum(wd.WorkDetWeight) from tworkdetails wd where wd.WorkID=twork.Uniq and wd.WorkDetCategory='PONDASI')*100) as Progress1,
      ((select sum(wd.WorkDetWeight) from tworkdetails wd where wd.WorkID=twork.Uniq and wd.WorkDetCategory='ERECTION' and wd.WorkDetIsComplete=1)/(select sum(wd.WorkDetWeight) from tworkdetails wd where wd.WorkID=twork.Uniq and wd.WorkDetCategory='ERECTION')*100) as Progress2,
      0 as ProgressDev1,
      0 as ProgressDev2,
      0 as ProgressStd1,
      0 as ProgressStd2,
      '' as DurBeton,
      ";
      $rwork = $this->db
      ->select($qselect)
      ->join(TBL_TPROJECT,TBL_TPROJECT.'.'.COL_UNIQ." = ".TBL_TWORK.".".COL_PRID,"inner")
      ->where(COL_PRID, $p[COL_UNIQ])
      ->where('EXISTS(select tw.Uniq from tworkdetails tw where tw.WorkID=twork.Uniq and tw.WorkDetIsComplete=0)')
      ->get(TBL_TWORK)
      ->result_array();

      foreach($rwork as $res) {
        $progress1 = round(toNum($res['Progress1']));
        $progress2 = round(toNum($res['Progress2']));
        $dev1 = round(toNum($res['ProgressDev1']));
        $dev2 = round(toNum($res['ProgressDev2']));
        $std1 = round(toNum($res['ProgressStd1']));
        $std2 = round(toNum($res['ProgressStd2']));

        if(!empty($res[COL_WORKSTARTDATE1]) && toNum($res['Progress1'])<100) {
          $rstd = $this->db
          ->where(COL_CATEGORY, 'PON')
          ->where(COL_TYPE1, $res[COL_WORKTYPE1])
          ->where(COL_TYPE2, $res[COL_WORKTYPE2])
          ->get(TBL_MSTANDARD)
          ->row_array();
          if($rstd) {
            $std = $rstd[COL_STANDARD];
            $dur = floor((time() - strtotime($res[COL_WORKSTARTDATE1])) / (60 * 60 * 24));
            $stdProgress = 0;
            if($std>0) {
              $stdProgress = ($dur/$std)*100;
            }
            $dev1 = round($stdProgress-$progress1);
          }
        }
        if(!empty($res[COL_WORKSTARTDATE2]) && toNum($res['Progress2'])<100) {
          $rstd = $this->db
          ->where(COL_CATEGORY, 'EREC')
          ->where(COL_TYPE1, $res[COL_WORKTYPE1])
          ->get(TBL_MSTANDARD)
          ->row_array();
          if($rstd) {
            $std = $rstd[COL_STANDARD];
            $dur = floor((time() - strtotime($res[COL_WORKSTARTDATE2])) / (60 * 60 * 24));
            $stdProgress = 0;
            if($std>0) {
              $stdProgress = ($dur/$std)*100;
            }
            $dev2 = round($stdProgress-$progress2);
          }
        }
        if(/*($ruser[COL_ROLEID]==ROLEADMIN || $ruser[COL_ROLEID]==ROLEPENGGUNA) && */($dev1>50 || $dev2>50)) {
          $arrnotifs[] = array(
            'WorkID'=>$res[COL_UNIQ],
            'WorkTitle'=>$res[COL_WORKTITLE],
            'PrID'=>$res[COL_PRID],
            'PrName'=>$res[COL_PRNAME],
            'MessageSeverity'=>'Deviasi > 50%',
            'MessageType'=>'Peringatan Berbahaya',
            'MessageText'=>'Terjadi keterlambatan progress pada Project <strong>'.$res[COL_PRNAME].'</strong> : <strong>'.$res[COL_WORKTITLE].'</strong> dengan deviasi > 50%'
          );
        }
        else if(/*($ruser[COL_ROLEID]==ROLEADMIN || $ruser[COL_ROLEID]==ROLEPENGAWAS) && */($dev1>25 || $dev2>25)) {
          if($ruser[COL_ROLEID]==ROLEMANAGER) {
            continue;
          }
          $arrnotifs[] = array(
            'WorkID'=>$res[COL_UNIQ],
            'WorkTitle'=>$res[COL_WORKTITLE],
            'PrID'=>$res[COL_PRID],
            'PrName'=>$res[COL_PRNAME],
            'MessageSeverity'=>'Deviasi > 25%',
            'MessageType'=>'Peringatan Dini',
            'MessageText'=>'Terjadi keterlambatan progress pada Project <strong>'.$res[COL_PRNAME].'</strong> : <strong>'.$res[COL_WORKTITLE].'</strong> dengan deviasi > 25%'
          );
        }

      }
    }
    ShowJsonSuccess('Loaded.', array('data'=>$arrnotifs));
  }

  public function get_list_member() {
    $res = $this->db
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__USERS.".".COL_USERNAME,"inner")
    ->where(COL_ROLEID, 3)
    ->get(TBL__USERS)
    ->result_array();
    ShowJsonSuccess('Loaded.', array('data'=>$res));
  }

  public function project_create() {
    $dat = array(
      COL_PRNAME=>$this->input->post(COL_PRNAME),
      COL_PRCONTRACTOR=>$this->input->post(COL_PRCONTRACTOR),
      COL_PRTRANS=>$this->input->post(COL_PRTRANS),
      COL_PRTRANSLENGTH=>$this->input->post(COL_PRTRANSLENGTH),
      COL_CREATEDBY=>$this->input->post(COL_USERNAME),
      COL_CREATEDON=>date('Y-m-d H:i:s')
    );
    $members = $this->input->post('Members');
    $memberArr = explode(",", $members);

    if(empty($memberArr)) {
      ShowJsonError('Harap memilih minimal 1 (satu) orang anggota.');
      exit();
    }

    $this->db->trans_begin();
    try {
      $res = $this->db->insert(TBL_TPROJECT, $dat);
      if(!$res) {
        throw new Exception('Terjadi kesalahan pada server.');
      }

      $id = $this->db->insert_id();
      foreach($memberArr as $m) {
        $res = $this->db->insert(TBL_TPROJECTMEMBER, array(
          COL_PRID=>$id,
          COL_USERNAME=>$m
        ));
        if(!$res) {
          throw new Exception('Terjadi kesalahan pada server.');
        }
      }
      $this->db->trans_commit();
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      exit();
    }

    $res = $this->db
    ->select('*, (select count(*) from twork work where work.PrID = tproject.Uniq) as NumDetail')
    ->where(COL_CREATEDBY, $this->input->post(COL_USERNAME))
    ->get(TBL_TPROJECT)
    ->result_array();
    ShowJsonSuccess('Project <strong>'.$dat[COL_PRNAME].'</strong> berhasil ditambahkan.', array('data'=>$res));
  }

  public function project_delete($uniq) {
    $res = $this->db->where(COL_UNIQ, $uniq)->update(TBL_TPROJECT, array(COL_ISDELETED=>1));
    if(!$res) {
      ShowJsonError('Gagal menghapus data.');
      exit();
    }

    ShowJsonSuccess('Data berhasil dihapus');
  }

  public function project_detail($uniq) {
    $uname = $this->input->post(COL_USERNAME);
    $ruser = $this->db
    ->where(COL_USERNAME,$uname)
    ->get(TBL__USERS)
    ->row_array();
    if(empty($ruser)) {
      ShowJsonError('Autentikasi gagal.');
      exit();
    }

    $res1 = $this->db
    ->where(COL_UNIQ, $uniq)
    ->get(TBL_TPROJECT)
    ->row_array();

    if(empty($res1)) {
      ShowJsonError('Project tidak ditemukan.');
      exit();
    }

    $res2 = $this->db
    ->select("*,
    ((select sum(wd.WorkDetWeight) from tworkdetails wd where wd.WorkID=twork.Uniq and wd.WorkDetCategory='PONDASI' and wd.WorkDetIsComplete=1)/(select sum(wd.WorkDetWeight) from tworkdetails wd where wd.WorkID=twork.Uniq and wd.WorkDetCategory='PONDASI')*100) as ProgressPondasi,
    ((select sum(wd.WorkDetWeight) from tworkdetails wd where wd.WorkID=twork.Uniq and wd.WorkDetCategory='ERECTION' and wd.WorkDetIsComplete=1)/(select sum(wd.WorkDetWeight) from tworkdetails wd where wd.WorkID=twork.Uniq and wd.WorkDetCategory='ERECTION')*100) as ProgressErection,
    '' as StatusCode,
    '' as StatusColor
    ")
    ->where(COL_PRID, $uniq)
    ->where(COL_ISDELETED, 0)
    ->order_by(COL_WORKSEQ, 'asc')
    ->order_by(COL_UNIQ, 'asc')
    ->get(TBL_TWORK)
    ->result_array();

    $res3 = $this->db
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TPROJECTMEMBER.".".COL_USERNAME,"inner")
    ->where(COL_PRID, $uniq)
    ->get(TBL_TPROJECTMEMBER)
    ->result_array();

    $isAuthorized = false;
    /*if($ruser[COL_ROLEID]!=ROLEADMIN&&$ruser[COL_ROLEID]!=ROLEMANAGER) {
      if($res1[COL_CREATEDBY]==$uname) $isAuthorized = true;
    } else {
      $isAuthorized = true;
    }*/
    if($ruser[COL_ROLEID]==ROLEADMIN || $ruser[COL_ROLEID]==ROLEMANAGER || $ruser[COL_ROLEID]==ROLEPENGGUNA) {
      $isAuthorized = true;
    }

    $progress = 0;
    $sumAll=0;
    $statusTowerSelesai=0;
    $statusPondasiSelesai=0;
    $statusErectionSelesai=0;
    $statusTowerBelum=0;
    $statusPondasiBelum=0;
    $statusErectionBelum=0;
    $statusPondasiStart=0;
    $statusErectionStart=0;
    $statusPondasiIdle=0;
    $statusErectionIdle=0;
    for($i=0; $i<count($res2); $i++) {
      $sumAll++;
      $betonUmur = 21;
      $betonDate = null;
      $betonDur = 0;
      if(!empty($res2[$i][COL_WORKDAYS3])) {
        $betonUmur = $res2[$i][COL_WORKDAYS3];
      }
      if(!empty($res2[$i][COL_WORKSTARTDATE3])) {
        $betonDate = date('Y-m-d', strtotime($res2[$i][COL_WORKSTARTDATE3]." +".$betonUmur." days"));
        $betonDur = floor((strtotime($betonDate) - time()) / (60 * 60 * 24));
      }

      if($res2[$i][COL_WORKSTATUS]=='BEBAS') {
        $statusTowerSelesai++;
      } else {
        $statusTowerBelum++;
      }

      if (toNum($res2[$i]['ProgressPondasi']) >= 100 && !empty($res2[$i][COL_WORKSTARTDATE1])) {
        $statusPondasiSelesai++;
      } else if(!empty($res2[$i][COL_WORKSTARTDATE1])) {
        $statusPondasiStart++;
      }

      if (toNum($res2[$i]['ProgressErection']) >= 100 && !empty($res2[$i][COL_WORKSTARTDATE2])) {
        $statusErectionSelesai++;
      } else if(!empty($res2[$i][COL_WORKSTARTDATE2])) {
        $statusErectionStart++;
      }

      if($res2[$i][COL_WORKSTATUS]=='BELUM BEBAS') {
        $res2[$i]['StatusCode'] = 1;
        $res2[$i]['StatusColor'] = '#F96666';
      } else if($res2[$i][COL_WORKSTATUS]=='BEBAS' && empty($res2[$i][COL_WORKSTARTDATE1])) {
        $res2[$i]['StatusCode'] = 2;
        $res2[$i]['StatusColor'] = '#F4E06D';
        $statusPondasiIdle++;
      } else if(!empty($res2[$i][COL_WORKSTARTDATE1]) && empty($res2[$i][COL_WORKSTARTDATE3])) {
        $res2[$i]['StatusCode'] = 3;
        $res2[$i]['StatusColor'] = '#5F9DF7';
      } else if(!empty($res2[$i][COL_WORKSTARTDATE3]) && $betonDur > 0) {
        $res2[$i]['StatusCode'] = 4;
        $res2[$i]['StatusColor'] = '#A66CFF';
      } else if($betonDur <= 0  && empty($res2[$i][COL_WORKSTARTDATE2])) {
        $res2[$i]['StatusCode'] = 5;
        $res2[$i]['StatusColor'] = '#FFA500';
        $statusErectionIdle++;
      } else if(!empty($res2[$i][COL_WORKSTARTDATE2]) && toNum($res2[$i]['ProgressErection'])<100) {
        $res2[$i]['StatusCode'] = 6;
        $res2[$i]['StatusColor'] = '#1F4690';
      } else if(toNum($res2[$i]['ProgressErection'])>=100) {
        $res2[$i]['StatusCode'] = 7;
        $res2[$i]['StatusColor'] = '#3D8361';
      }
    }

    $res1 = array_merge($res1, array(
      'progressPercentage'=>$sumAll>0?($statusErectionSelesai/$sumAll)*100:0,
      'sumAll'=>$sumAll,
      'sumTowerSelesai'=>$statusTowerSelesai,
      'sumPondasiSelesai'=>$statusPondasiSelesai,
      'sumErectionSelesai'=>$statusErectionSelesai,
      'sumTowerBelum'=>$statusTowerBelum,
      'sumPondasiBelum'=>$sumAll-$statusPondasiSelesai,
      'sumErectionBelum'=>$sumAll-$statusErectionSelesai,
      'sumPondasiOngoing'=>$statusPondasiStart,
      'sumErectionOngoing'=>$statusErectionStart,
      'sumPondasiAvailable'=>$statusPondasiIdle,
      'sumErectionAvailable'=>$statusErectionIdle,
    ));

    ShowJsonSuccess('Loaded.', array('data'=>array('project'=>$res1,'items'=>$res2,'members'=>$res3,'isAuthorized'=>$isAuthorized)));
  }

  public function tower_create($prid) {
    $rworkdetail = $this->db
    ->where(COL_WORKTYPE, $this->input->post(COL_WORKTYPE2))
    ->get(TBL_MWORKDETAIL)
    ->result_array();

    $dat = array(
      COL_PRID=>$prid,
      COL_WORKTITLE=>$this->input->post(COL_WORKTITLE),
      COL_WORKCOORD=>$this->input->post(COL_WORKCOORD),
      COL_WORKTYPE1=>$this->input->post(COL_WORKTYPE1),
      COL_WORKTYPE2=>$this->input->post(COL_WORKTYPE2),
      COL_WORKEXT=>$this->input->post(COL_WORKEXT),
      COL_CREATEDBY=>$this->input->post(COL_CREATEDBY),
      COL_CREATEDON=>date('Y-m-d H:i:s')
    );

    $this->db->trans_begin();
    try {
      $res = $this->db->insert(TBL_TWORK, $dat);
      if(!$res) {
        throw new Exception('Terjadi kesalahan pada server.');
      }

      $id = $this->db->insert_id();
      $arrDetail = array();
      foreach ($rworkdetail as $r) {
        $arrDetail[] = array(
          COL_WORKID=>$id,
          COL_WORKDETCATEGORY=>$r[COL_WORKDETCATEGORY],
          COL_WORKDETNAME=>$r[COL_WORKDETNAME],
          COL_WORKDETTYPE=>$r[COL_WORKDETTYPE],
          COL_WORKDETWEIGHT=>$r[COL_WORKDETWEIGHT]
        );
      }
      if(!empty($arrDetail)) {
        $res = $this->db->insert_batch(TBL_TWORKDETAILS, $arrDetail);
        if(!$res) {
          throw new Exception('Terjadi kesalahan pada server.');
        }
      }
      $this->db->trans_commit();
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      exit();
    }
    ShowJsonSuccess('Tower <strong>'.$dat[COL_WORKTITLE].'</strong> berhasil ditambahkan.');
  }

  public function tower_detail($uniq) {
    $uname = $this->input->post(COL_USERNAME);
    $ruser = $this->db
    ->where(COL_USERNAME,$uname)
    ->get(TBL__USERS)
    ->row_array();
    if(empty($ruser)) {
      ShowJsonError('Autentikasi gagal.');
      exit();
    }

    $qselect = @"
    twork.*,tproject.CreatedBy as PrCreator,
    ((select sum(wd.WorkDetWeight) from tworkdetails wd where wd.WorkID=twork.Uniq and wd.WorkDetCategory='PONDASI' and wd.WorkDetIsComplete=1)/(select sum(wd.WorkDetWeight) from tworkdetails wd where wd.WorkID=twork.Uniq and wd.WorkDetCategory='PONDASI')*100) as Progress1,
    ((select sum(wd.WorkDetWeight) from tworkdetails wd where wd.WorkID=twork.Uniq and wd.WorkDetCategory='ERECTION' and wd.WorkDetIsComplete=1)/(select sum(wd.WorkDetWeight) from tworkdetails wd where wd.WorkID=twork.Uniq and wd.WorkDetCategory='ERECTION')*100) as Progress2,
    0 as ProgressDev1,
    0 as ProgressDev2,
    0 as ProgressStd1,
    0 as ProgressStd2,
    '' as DurBeton,
    ";
    $res = $this->db
    ->select($qselect)
    ->join(TBL_TPROJECT,TBL_TPROJECT.'.'.COL_UNIQ." = ".TBL_TWORK.".".COL_PRID,"inner")
    ->where(TBL_TWORK.'.'.COL_UNIQ, $uniq)
    ->get(TBL_TWORK)
    ->row_array();

    if(empty($res)) {
      ShowJsonError('Data tidak ditemukan.');
      exit();
    }

    $res3 = $this->db
    ->where(COL_PRID, $res[COL_PRID])
    ->get(TBL_TPROJECTMEMBER)
    ->result_array();

    $isAuthorized = false;
    /*if($ruser[COL_ROLEID]!=ROLEADMIN&&$ruser[COL_ROLEID]!=ROLEMANAGER) {
      if($res['PrCreator']==$uname) $isAuthorized = true;
      foreach($res3 as $r) {
        if($r[COL_USERNAME]==$uname) $isAuthorized = true;
      }
    } else {
      $isAuthorized = true;
    }*/
    if($ruser[COL_ROLEID]==ROLEADMIN || $ruser[COL_ROLEID]==ROLEMANAGER || $ruser[COL_ROLEID]==ROLEPENGGUNA) {
      $isAuthorized = true;
    } else {
      foreach($res3 as $r) {
        if($r[COL_USERNAME]==$uname) $isAuthorized = true;
      }
    }

    $res['Progress1'] = round(toNum($res['Progress1']),2);
    $res['Progress2'] = round(toNum($res['Progress2']),2);
    $res['ProgressDev1'] = round(toNum($res['ProgressDev1']),2);
    $res['ProgressDev2'] = round(toNum($res['ProgressDev2']),2);
    $res['ProgressStd1'] = round(toNum($res['ProgressStd1']),2);
    $res['ProgressStd2'] = round(toNum($res['ProgressStd2']),2);
    if(!empty($res[COL_WORKSTARTDATE1])) {
      $rstd = $this->db
      ->where(COL_CATEGORY, 'PON')
      ->where(COL_TYPE1, $res[COL_WORKTYPE1])
      ->where(COL_TYPE2, $res[COL_WORKTYPE2])
      ->get(TBL_MSTANDARD)
      ->row_array();
      if($rstd) {
        $std = $rstd[COL_STANDARD];
        $dur = floor((time() - strtotime($res[COL_WORKSTARTDATE1])) / (60 * 60 * 24));
        $stdProgress = 0;
        if($std>0) {
          $stdProgress = ($dur/$std)*100;
          $res['ProgressStd1'] = round($stdProgress,2);
        }
        if($res['Progress1'] < 100) {
          $res['ProgressDev1'] = $stdProgress-$res['Progress1']>0 ? round($stdProgress-$res['Progress1'],2) : 0;
        }
      }
    }
    if(!empty($res[COL_WORKSTARTDATE2])) {
      $rstd = $this->db
      ->where(COL_CATEGORY, 'EREC')
      ->where(COL_TYPE1, $res[COL_WORKTYPE1])
      ->get(TBL_MSTANDARD)
      ->row_array();
      if($rstd) {
        $std = $rstd[COL_STANDARD];
        $dur = floor((time() - strtotime($res[COL_WORKSTARTDATE2])) / (60 * 60 * 24));
        $stdProgress = 0;
        if($std>0) {
          $stdProgress = ($dur/$std)*100;
          $res['ProgressStd2'] = round($stdProgress,2);
        }
        if($res['Progress2'] < 100) {
          $res['ProgressDev2'] = $stdProgress-$res['Progress2']>0 ? round($stdProgress-$res['Progress2'],2) : 0;
        }
      }
    }

    if(!empty($res[COL_WORKSTARTDATE3])) {
      $umur = 21;
      if(!empty($res[COL_WORKDAYS3])) {
        $umur = $res[COL_WORKDAYS3];
      }
      $dateFinish = date('Y-m-d', strtotime($res[COL_WORKSTARTDATE3]." +".$umur." days"));
      $res['DurBeton'] = floor((strtotime($dateFinish) - time()) / (60 * 60 * 24)) > 0 ? floor((strtotime($dateFinish) - time()) / (60 * 60 * 24)) : 0;
    }

    $res2 = $this->db
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TWORKREPORTS.".".COL_CREATEDBY,"inner")
    ->where(COL_WORKID, $uniq)
    ->order_by(COL_CREATEDON, 'desc')
    ->get(TBL_TWORKREPORTS)
    ->result_array();

    ShowJsonSuccess('Loaded.', array('data'=>$res,'reports'=>$res2,'isAuthorized'=>$isAuthorized));
  }

  public function tower_change($uniq) {
    $rdata = $this->db
    ->where(COL_UNIQ, $uniq)
    ->get(TBL_TWORK)
    ->row_array();

    if(empty($rdata)) {
      ShowJsonError('Project tidak ditemukan.');
      exit();
    }

    $prop = $this->input->post('prop');
    $val = $this->input->post('val');
    if($prop==COL_WORKISADIKTIF) {
      if($val=='Ya') $val=1;
      else $val=0;
    }

    $arrUpdate = array($prop=>!empty($val)?$val:null);
    if($prop==COL_WORKISADIKTIF) {
      $arrUpdate = array($prop=>!empty($val)?$val:null, COL_WORKDAYS3=>$val==1?7:21);
    }
    if(($prop==COL_WORKSTARTDATE1||$prop==COL_WORKSTARTDATE2||$prop==COL_WORKSTARTDATE3) && $rdata[COL_WORKSTATUS]!='BEBAS') {
      ShowJsonError('Tanggal tidak bisa diubah karena status lahan BELUM BEBAS!');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $uniq)->update(TBL_TWORK, $arrUpdate);
    if(!$res) {
      ShowJsonError('Terjadi kesalahan pada server.');
      exit();
    }

    ShowJsonSuccess($rdata[COL_WORKTITLE].' berhasil diperbarui.');
  }

  public function tower_report_add($uniq) {
    $rdata = $this->db
    ->where(COL_UNIQ, $uniq)
    ->get(TBL_TWORK)
    ->row_array();

    if(empty($rdata)) {
      ShowJsonError('Project tidak ditemukan.');
      exit();
    }

    $username = $this->input->post('UserName');
    $message = $this->input->post('WorkRepMessage');

    $res = $this->db->insert(TBL_TWORKREPORTS, array(COL_WORKID=>$uniq,COL_WORKREPMESSAGE=>$message,COL_CREATEDBY=>$username,COL_CREATEDON=>date('Y-m-d H:i:s')));
    if(!$res) {
      ShowJsonError('Terjadi kesalahan pada server.');
      exit();
    }

    ShowJsonSuccess('Laporan berhasil ditambahkan.');
  }

  public function tower_delete($uniq) {
    $res = $this->db->where(COL_UNIQ, $uniq)->update(TBL_TWORK, array(COL_ISDELETED=>1));
    if(!$res) {
      ShowJsonError('Gagal menghapus data.');
      exit();
    }

    ShowJsonSuccess('Data berhasil dihapus');
  }

  public function tower_progress($id, $tipe) {
    $uname = $this->input->post(COL_USERNAME);
    $ruser = $this->db
    ->where(COL_USERNAME,$uname)
    ->get(TBL__USERS)
    ->row_array();
    if(empty($ruser)) {
      ShowJsonError('Autentikasi gagal.');
      exit();
    }

    $res = $this->db
    ->select('twork.*, tproject.CreatedBy as PrCreator')
    ->join(TBL_TPROJECT,TBL_TPROJECT.'.'.COL_UNIQ." = ".TBL_TWORK.".".COL_PRID,"inner")
    ->where(TBL_TWORK.'.'.COL_UNIQ, $id)
    ->get(TBL_TWORK)
    ->row_array();

    if(empty($res)) {
      ShowJsonError('Data tidak ditemukan.');
      exit();
    }

    $res3 = $this->db
    ->where(COL_PRID, $res[COL_PRID])
    ->get(TBL_TPROJECTMEMBER)
    ->result_array();

    $isAuthorized = false;
    /*if($ruser[COL_ROLEID]!=ROLEADMIN&&$ruser[COL_ROLEID]!=ROLEMANAGER) {
      if($res['PrCreator']==$uname) $isAuthorized = true;
      foreach($res3 as $r) {
        if($r[COL_USERNAME]==$uname) $isAuthorized = true;
      }
    } else {
      $isAuthorized = true;
    }*/
    if($ruser[COL_ROLEID]==ROLEADMIN || $ruser[COL_ROLEID]==ROLEMANAGER || $ruser[COL_ROLEID]==ROLEPENGGUNA) {
      $isAuthorized = true;
    } else {
      foreach($res3 as $r) {
        if($r[COL_USERNAME]==$uname) $isAuthorized = true;
      }
    }

    $rkeys = $this->db
    ->select(COL_WORKDETTYPE)
    ->where(COL_WORKID, $id)
    ->where(COL_WORKDETCATEGORY, strtoupper($tipe))
    ->order_by(COL_UNIQ)
    ->group_by(COL_WORKDETTYPE)
    ->get(TBL_TWORKDETAILS)
    ->result_array();

    $rdet = $this->db
    ->where(COL_WORKID, $id)
    ->where(COL_WORKDETCATEGORY, strtoupper($tipe))
    ->get(TBL_TWORKDETAILS)
    ->result_array();

    for($i=0; $i<count($rdet); $i++) {
      if(!empty($rdet[$i][COL_WORKDETIMAGES])) {
        $arrImg = explode(",",$rdet[$i][COL_WORKDETIMAGES]);
        $arrImg_ = array();
        foreach($arrImg as $img) {
          $arrImg_[] = MY_UPLOADURL.$img;
        }
        $rdet[$i][COL_WORKDETIMAGES] = implode(",", $arrImg_);
      }
    }

    ShowJsonSuccess('Loaded.', array('data'=>$res,'keys'=>$rkeys,'items'=>$rdet,'isAuthorized'=>$isAuthorized));
  }

  public function tower_progress_changestatus($uniq) {
    $rdata = $this->db
    ->where(COL_UNIQ, $uniq)
    ->get(TBL_TWORKDETAILS)
    ->row_array();

    if(empty($rdata)) {
      ShowJsonError('Data tidak ditemukan.');
      exit();
    }

    $rwork = $this->db
    ->where(COL_UNIQ, $rdata[COL_WORKID])
    ->get(TBL_TWORK)
    ->row_array();

    if(empty($rwork)) {
      ShowJsonError('Data tidak ditemukan.');
      exit();
    }

    $res = $this->db
    ->where(COL_UNIQ, $uniq)
    ->update(TBL_TWORKDETAILS, array(COL_WORKDETISCOMPLETE=>($rdata[COL_WORKDETISCOMPLETE]==1?0:1)));
    if(!$res) {
      ShowJsonError('Terjadi kesalahan pada server.');
      exit();
    }

    /* Check & Update Pondasi Complete */
    $isPondasiComplete = false;
    if($rdata[COL_WORKDETCATEGORY]=='PONDASI') {
      $rcheck = $this->db
      ->where(COL_WORKID, $rdata[COL_WORKID])
      ->where(COL_WORKDETCATEGORY, 'PONDASI')
      ->where(COL_WORKDETISCOMPLETE, 0)
      ->get(TBL_TWORKDETAILS)
      ->row_array();
      if(empty($rcheck) && empty($rwork[COL_WORKSTARTDATE3])) {
        $isPondasiComplete = true;
      } else {
        $res = $this->db
        ->where(COL_UNIQ, $rdata[COL_WORKID])
        ->update(TBL_TWORK, array(COL_WORKDAYS3=>null, COL_WORKISADIKTIF=>null));
      }
    }

    /* Check & Update Pondasi Complete */

    ShowJsonSuccess('Status '.$rdata[COL_WORKDETTYPE].' - '.$rdata[COL_WORKDETNAME].' berhasil diperbarui.', array('data'=>array('WorkID'=>$rdata[COL_WORKID], 'isPondasiComplete'=>$isPondasiComplete)));
  }

  public function tower_update_umurbeton($uniq) {
    $rdata = $this->db
    ->where(COL_UNIQ, $uniq)
    ->get(TBL_TWORK)
    ->row_array();

    if(empty($rdata)) {
      ShowJsonError('Data tidak ditemukan.');
      exit();
    }

    $isAdiktif = $this->input->post('WorkIsAdiktif');
    $umurbeton = 0;
    if($isAdiktif && toNum($isAdiktif) == 1) {
      $umurbeton = 7;
    } else {
      $umurbeton = 21;
    }

    $res = $this->db
    ->where(COL_UNIQ, $uniq)
    ->update(TBL_TWORK, array(COL_WORKSTARTDATE3=>date('Y-m-d H:i:s'), COL_WORKDAYS3=>$umurbeton, COL_WORKISADIKTIF=>$isAdiktif));
    if(!$res) {
      ShowJsonError('Terjadi kesalahan pada server.');
      exit();
    }

    ShowJsonSuccess('Unggah gambar berhasil');
    exit();
  }

  public function tower_progress_upload($uniq) {
    $rdata = $this->db
    ->where(COL_UNIQ, $uniq)
    ->get(TBL_TWORKDETAILS)
    ->row_array();

    if(empty($rdata)) {
      ShowJsonError('Data tidak ditemukan.');
      exit();
    }

    $config['upload_path'] = MY_UPLOADPATH;
    $config['allowed_types'] = "gif|jpg|jpeg|png|pdf|mp4|avi|mkv|wav|webm|flv|wmv|mov";
    $config['overwrite'] = FALSE;
    $this->load->library('upload',$config);
    if(!empty($_FILES['file'])) {
      $res = $this->upload->do_upload('file');
      if(!$res) {
        ShowJsonError(strip_tags($this->upload->display_errors()));
        exit();
      }

      $upl = $this->upload->data();
      $currImg = $rdata[COL_WORKDETIMAGES];
      $arrImg = array();
      if(!empty($currImg)) {
        $arrImg = explode(",", $currImg);
        $arrImg[] = $upl['file_name'];
        $currImg = implode(",", $arrImg);
      } else {
        $currImg = $upl['file_name'];
      }
      $res = $this->db->where(COL_UNIQ, $uniq)->update(TBL_TWORKDETAILS, array(COL_WORKDETIMAGES=>$currImg));
      if(!$res) {
        ShowJsonError('Terjadi kesalahan pada server.');
        exit();
      }

      ShowJsonSuccess('Unggah gambar berhasil');
      exit();
    } else {
      ShowJsonError('Gagal mengunggah gambar.');
      exit();
    }
  }

  public function tower_progress_imgdel($uniq) {
    $rdata = $this->db
    ->where(COL_UNIQ, $uniq)
    ->get(TBL_TWORKDETAILS)
    ->row_array();

    if(empty($rdata)) {
      ShowJsonError('Data tidak ditemukan.');
      exit();
    }

    $idx = $this->input->post('idx');
    $arrImg = array();
    if(!empty($rdata[COL_WORKDETIMAGES])) {
      $arrImg = explode(",", $rdata[COL_WORKDETIMAGES]);
    }

    if(isset($arrImg[$idx])) {
      if(file_exists(MY_UPLOADPATH.$arrImg[$idx])) {
        unlink(MY_UPLOADPATH.$arrImg[$idx]);
      }
      array_splice($arrImg, $idx, 1);
    }

    $res = $this->db->where(COL_UNIQ, $uniq)->update(TBL_TWORKDETAILS, array(COL_WORKDETIMAGES=>implode(",",$arrImg)));
    if(!$res) {
      ShowJsonError('Terjadi kesalahan pada server.');
      exit();
    }

    ShowJsonSuccess('Berhasil menghapus.');
    exit();
  }

  public function tower_seq_update($uniq) {
    $from = $this->input->post('from');
    $to = $this->input->post('to');

    $rdata = $this->db
    ->where(COL_PRID, $uniq)
    ->where(COL_ISDELETED, 0)
    ->order_by(COL_WORKSEQ, 'asc')
    ->order_by(COL_UNIQ, 'asc')
    ->get(TBL_TWORK)
    ->result_array();

    $arrcurrent = array();
    foreach($rdata as $r) {
      $arrcurrent[] = $r;
    }

    $arrShift = array_splice($arrcurrent, $from, 1);
    array_splice($arrcurrent, $to, 0, $arrShift);

    $this->db->trans_begin();
    try {
      for($i=0; $i<count($arrcurrent); $i++) {
        $res = $this->db->where(COL_UNIQ, $arrcurrent[$i][COL_UNIQ])->update(TBL_TWORK, array(COL_WORKSEQ=>$i));
        if(!$res) {
          throw new Exception('Terjadi kesalahan pada server.');
        }
      }
      $this->db->trans_commit();
      ShowJsonSuccess('Berhasil.');
      exit();
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      exit();
    }
  }

  public function report($id) {
    $res1 = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TPROJECT)
    ->row_array();

    if(empty($res1)) {
      ShowJsonError('Project tidak ditemukan.');
      exit();
    }

    $res2 = $this->db
    ->select("*,
    ((select sum(wd.WorkDetWeight) from tworkdetails wd where wd.WorkID=twork.Uniq and wd.WorkDetCategory='PONDASI' and wd.WorkDetIsComplete=1)/(select sum(wd.WorkDetWeight) from tworkdetails wd where wd.WorkID=twork.Uniq and wd.WorkDetCategory='PONDASI')*100) as Progress1,
    ((select sum(wd.WorkDetWeight) from tworkdetails wd where wd.WorkID=twork.Uniq and wd.WorkDetCategory='ERECTION' and wd.WorkDetIsComplete=1)/(select sum(wd.WorkDetWeight) from tworkdetails wd where wd.WorkID=twork.Uniq and wd.WorkDetCategory='ERECTION')*100) as Progress2,
    '' as StatusCode,
    '' as StatusColor,
    0 as ProgressDev1,
    0 as ProgressDev2,
    0 as ProgressStd1,
    0 as ProgressStd2,
    ")
    ->where(COL_PRID, $id)
    ->where(COL_ISDELETED, 0)
    ->order_by(COL_WORKSEQ, 'asc')
    ->order_by(COL_UNIQ, 'asc')
    ->get(TBL_TWORK)
    ->result_array();

    $progress = 0;
    $sumAll=0;
    $statusTowerSelesai=0;
    $statusPondasiSelesai=0;
    $statusErectionSelesai=0;
    $statusTowerBelum=0;
    $statusPondasiBelum=0;
    $statusErectionBelum=0;
    $statusPondasiStart=0;
    $statusErectionStart=0;
    $statusPondasiIdle=0;
    $statusErectionIdle=0;
    for($i=0; $i<count($res2); $i++) {
      $sumAll++;
      $betonUmur = 21;
      $betonDate = null;
      $betonDur = 0;
      if(!empty($res2[$i][COL_WORKDAYS3])) {
        $betonUmur = $res2[$i][COL_WORKDAYS3];
      }
      if(!empty($res2[$i][COL_WORKSTARTDATE3])) {
        $betonDate = date('Y-m-d', strtotime($res2[$i][COL_WORKSTARTDATE3]." +".$betonUmur." days"));
        $betonDur = floor((strtotime($betonDate) - time()) / (60 * 60 * 24));
      }

      if($res2[$i][COL_WORKSTATUS]=='BEBAS') {
        $statusTowerSelesai++;
      } else {
        $statusTowerBelum++;
      }

      if (toNum($res2[$i]['Progress1']) >= 100 && !empty($res2[$i][COL_WORKSTARTDATE1])) {
        $statusPondasiSelesai++;
      } else if(!empty($res2[$i][COL_WORKSTARTDATE1])) {
        $statusPondasiStart++;
      }

      if (toNum($res2[$i]['Progress2']) >= 100 && !empty($res2[$i][COL_WORKSTARTDATE2])) {
        $statusErectionSelesai++;
      } else if(!empty($res2[$i][COL_WORKSTARTDATE2])) {
        $statusErectionStart++;
      }

      if($res2[$i][COL_WORKSTATUS]=='BELUM BEBAS') {
        $res2[$i]['StatusCode'] = 1;
        $res2[$i]['StatusColor'] = '#F96666';
      } else if($res2[$i][COL_WORKSTATUS]=='BEBAS' && empty($res2[$i][COL_WORKSTARTDATE1])) {
        $res2[$i]['StatusCode'] = 2;
        $res2[$i]['StatusColor'] = '#F4E06D';
        $statusPondasiIdle++;
      } else if(!empty($res2[$i][COL_WORKSTARTDATE1]) && empty($res2[$i][COL_WORKSTARTDATE3])) {
        $res2[$i]['StatusCode'] = 3;
        $res2[$i]['StatusColor'] = '#5F9DF7';
      } else if(!empty($res2[$i][COL_WORKSTARTDATE3]) && $betonDur > 0) {
        $res2[$i]['StatusCode'] = 4;
        $res2[$i]['StatusColor'] = '#A66CFF';
      } else if($betonDur <= 0  && empty($res2[$i][COL_WORKSTARTDATE2])) {
        $res2[$i]['StatusCode'] = 5;
        $res2[$i]['StatusColor'] = '#FFA500';
        $statusErectionIdle++;
      } else if(!empty($res2[$i][COL_WORKSTARTDATE2]) && toNum($res2[$i]['Progress2'])<100) {
        $res2[$i]['StatusCode'] = 6;
        $res2[$i]['StatusColor'] = '#1F4690';
      } else if(toNum($res2[$i]['Progress2'])>=100) {
        $res2[$i]['StatusCode'] = 7;
        $res2[$i]['StatusColor'] = '#3D8361';
      }

      $res2[$i]['Progress1'] = round(toNum($res2[$i]['Progress1']),2);
      $res2[$i]['Progress2'] = round(toNum($res2[$i]['Progress2']),2);
      $res2[$i]['ProgressDev1'] = round(toNum($res2[$i]['ProgressDev1']),2);
      $res2[$i]['ProgressDev2'] = round(toNum($res2[$i]['ProgressDev2']),2);
      $res2[$i]['ProgressStd1'] = round(toNum($res2[$i]['ProgressStd1']),2);
      $res2[$i]['ProgressStd2'] = round(toNum($res2[$i]['ProgressStd2']),2);
      if(!empty($res2[$i][COL_WORKSTARTDATE1])) {
        $rstd = $this->db
        ->where(COL_CATEGORY, 'PON')
        ->where(COL_TYPE1, $res2[$i][COL_WORKTYPE1])
        ->where(COL_TYPE2, $res2[$i][COL_WORKTYPE2])
        ->get(TBL_MSTANDARD)
        ->row_array();
        if($rstd) {
          $std = $rstd[COL_STANDARD];
          $dur = floor((time() - strtotime($res2[$i][COL_WORKSTARTDATE1])) / (60 * 60 * 24));
          $stdProgress = 0;
          if($std>0) {
            $stdProgress = ($dur/$std)*100;
            $res2[$i]['ProgressStd1'] = round($stdProgress,2);
          }
          if($res2[$i]['Progress1'] < 100) {
            $res2[$i]['ProgressDev1'] = $stdProgress-$res2[$i]['Progress1']>0 ? round($stdProgress-$res2[$i]['Progress1'],2) : 0;
          }
        }
      }
      if(!empty($res2[$i][COL_WORKSTARTDATE2])) {
        $rstd = $this->db
        ->where(COL_CATEGORY, 'EREC')
        ->where(COL_TYPE1, $res2[$i][COL_WORKTYPE1])
        ->get(TBL_MSTANDARD)
        ->row_array();
        if($rstd) {
          $std = $rstd[COL_STANDARD];
          $dur = floor((time() - strtotime($res2[$i][COL_WORKSTARTDATE2])) / (60 * 60 * 24));
          $stdProgress = 0;
          if($std>0) {
            $stdProgress = ($dur/$std)*100;
            $res2[$i]['ProgressStd2'] = round($stdProgress,2);
          }
          if($res2[$i]['Progress2'] < 100) {
            $res2[$i]['ProgressDev2'] = $stdProgress-$res2[$i]['Progress2']>0 ? round($stdProgress-$res2[$i]['Progress2'],2) : 0;
          }
        }
      }
    }

    $res1 = array_merge($res1, array(
      'progressPercentage'=>$sumAll>0?($statusErectionSelesai/$sumAll)*100:0,
      'sumAll'=>$sumAll,
      'sumTowerSelesai'=>$statusTowerSelesai,
      'sumPondasiSelesai'=>$statusPondasiSelesai,
      'sumErectionSelesai'=>$statusErectionSelesai,
      'sumTowerBelum'=>$statusTowerBelum,
      'sumPondasiBelum'=>$sumAll-$statusPondasiSelesai,
      'sumErectionBelum'=>$sumAll-$statusErectionSelesai,
      'sumPondasiOngoing'=>$statusPondasiStart,
      'sumErectionOngoing'=>$statusErectionStart,
      'sumPondasiAvailable'=>$statusPondasiIdle,
      'sumErectionAvailable'=>$statusErectionIdle,
    ));

    $this->load->library('Mypdf');
    $mpdf = new Mypdf();

    $html = $this->load->view('project/cetak', array('project'=>$res1, 'tower'=>$res2), TRUE);
    //echo $html;
    //return;
    $mpdf->pdf->SetTitle('Laporan Monitoring - '.$res1[COL_PRNAME]);
    $mpdf->pdf->SetFooter('Dicetak melalui aplikasi '.$this->setting_web_name);
    $mpdf->pdf->WriteHTML($html);
    $mpdf->pdf->Output('Laporan Monitoring - '.$res1[COL_PRNAME].'.pdf', 'D');
  }
}
