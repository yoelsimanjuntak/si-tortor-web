<section class="news-single section" style="background: #f9f9f9 !important; padding-top: 30px !important">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="single-main">
          <!-- News Title -->
          <h1 class="news-title"><?=($data[COL_UNITTIPE]=='PUSKESMAS'?'PUSKESMAS ':'').strtoupper($data[COL_UNITNAMA])?></h1>
          <!-- Meta -->
          <div class="meta">
            <div class="meta-left">
              <span class="author"><i class="far fa-user"></i>&nbsp;--</span>
              <span class="author"><i class="far fa-clock"></i>&nbsp;--</span>
            </div>
            <div class="meta-right" style="margin-top: 0 !important">
              <span class="views"><i class="fa fa-eye"></i>--</span>
            </div>
          </div>
          <!-- News Text -->
          <div class="news-text">--</div>
        </div>
      </div>

    </div>
  </div>
</section>
