<html>
<head>
  <title>Laporan Monitoring - <?=$project[COL_PRNAME]?></title>
  <style>
  body {
    font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
  }
  th, td {
    padding: 5px;
  }
  </style>
</head>
<body>
  <table width="100%">
    <tr>
      <td colspan="3" style="text-align: center; vertical-align: middle">
        <h4 style="font-weight: normal">LAPORAN MONITORING PROGRES PEKERJAAN</h4>
        <h4><?=strtoupper($project[COL_PRNAME])?></h4>
        <hr />
      </td>
    </tr>
    <tr>
      <td style="width: 100px; white-space: nowrap">Tanggal</td>
      <td style="width: 10px; white-space: nowrap">:</td>
      <td style="font-weight: bold"><?=date('d-m-Y')?></td>
    </tr>
  </table>
  <br />
  <h4 style="text-align: center">REKAPITULASI</h4>
  <table style="border: 1px solid #000; border-spacing: 0" border="1" width="100%">
    <tr style="background-color: #3B9AE1;">
      <th></th>
      <th style="color: #fff !important">TAPAK TOWER</th>
      <th style="color: #fff !important">PONDASI</th>
      <th style="color: #fff !important">ERECTION</th>
    </tr>
    <tr>
      <th style="text-align: left !important">TOTAL TOWER</th>
      <td colspan="3" style="text-align: center"><?=$project['sumAll']?></td>
    </tr>
    <tr>
      <th style="text-align: left !important">SELESAI</th>
      <td style="text-align: center"><?=$project['sumTowerSelesai']?></td>
      <td style="text-align: center"><?=$project['sumPondasiSelesai']?></td>
      <td style="text-align: center"><?=$project['sumErectionSelesai']?></td>
    </tr>
    <tr>
      <th style="text-align: left !important">BELUM</th>
      <td style="text-align: center"><?=$project['sumTowerBelum']?></td>
      <td style="text-align: center"><?=$project['sumPondasiBelum']?></td>
      <td style="text-align: center"><?=$project['sumErectionBelum']?></td>
    </tr>
    <tr>
      <th style="text-align: left !important">ON GOING</th>
      <td style="text-align: center"></td>
      <td style="text-align: center"><?=$project['sumPondasiOngoing']?></td>
      <td style="text-align: center"><?=$project['sumErectionOngoing']?></td>
    </tr>
    <tr>
      <th style="text-align: left !important">AVAILABLE</th>
      <td style="text-align: center"></td>
      <td style="text-align: center"><?=$project['sumPondasiAvailable']?></td>
      <td style="text-align: center"><?=$project['sumErectionAvailable']?></td>
    </tr>
  </table>
  <pagebreak></pagebreak>
  <table width="100%">
    <tr>
      <td colspan="3" style="text-align: center; vertical-align: middle">
        <h4 style="font-weight: normal">LAPORAN MONITORING PROGRES PEKERJAAN</h4>
        <h4><?=strtoupper($project[COL_PRNAME])?></h4>
        <hr />
      </td>
    </tr>
    <tr>
      <td style="width: 100px; white-space: nowrap">Tanggal</td>
      <td style="width: 10px; white-space: nowrap">:</td>
      <td style="font-weight: bold"><?=date('d-m-Y')?></td>
    </tr>
  </table>
  <br />
  <h4 style="text-align: center">RINCIAN PROGRESS</h4>
  <table style="border: 1px solid #000; border-spacing: 0" border="1" width="100%">
    <thead>
      <tr>
        <th rowspan="2">No.</th>
        <th rowspan="2">Nama Tower</th>
        <th rowspan="2">Jenis</th>
        <th rowspan="2">Pondasi</th>
        <th colspan="3">Status Tower</th>
      </tr>
      <tr>
        <th>Status</th>
        <th>Progress</th>
        <th>Deviasi</th>
      </tr>
    </thead>
    <?php
    $no = 1;
    $arrOngoing = array();
    foreach($tower as $r) {
      $stat = '';
      if($r['StatusCode']==1) $stat='BELUM BEBAS';
      else if($r['StatusCode']==2) $stat='IDLE PONDASI';
      else if($r['StatusCode']==3) $stat='ONGOING PONDASI';
      else if($r['StatusCode']==4) $stat='MASA TUNGGU UMUR BETON';
      else if($r['StatusCode']==5) $stat='IDLE ERECTION';
      else if($r['StatusCode']==6) $stat='ONGOING ERECTION';
      else if($r['StatusCode']==7) $stat='SELESAI';
      ?>
      <tr>
        <td style="text-align: right"><?=$no?>.</td>
        <td><?=$r[COL_WORKTITLE]?></td>
        <td><?=$r[COL_WORKTYPE1]?> +<?=$r[COL_WORKEXT]?></td>
        <td>Kelas <?=$r[COL_WORKTYPE2]?></td>
        <td><?=$stat?></td>
        <td style="text-align: right"><?=$r['StatusCode']==3?($r['Progress1'].'%'):($r['StatusCode']==6?$r['Progress2'].'%':'-')?></td>
        <td style="text-align: right"><?=$r['StatusCode']==3?$r['ProgressDev1'].'%':($r['StatusCode']==6?$r['ProgressDev2'].'%':'-')?></td>
      </tr>
      <?php
      $no++;
      if($r['StatusCode']==3||$r['StatusCode']==6) {
        $arrOngoing[] = $r;
      }
    }
    ?>
  </table>
  <pagebreak></pagebreak>
  <table width="100%">
    <tr>
      <td colspan="3" style="text-align: center; vertical-align: middle">
        <h4 style="font-weight: normal">LAPORAN MONITORING PROGRES PEKERJAAN</h4>
        <h4><?=strtoupper($project[COL_PRNAME])?></h4>
        <hr />
      </td>
    </tr>
    <tr>
      <td style="width: 100px; white-space: nowrap">Tanggal</td>
      <td style="width: 10px; white-space: nowrap">:</td>
      <td style="font-weight: bold"><?=date('d-m-Y')?></td>
    </tr>
  </table>
  <br />
  <h4 style="text-align: center">PEKERJAAN ON-GOING</h4>
  <table style="border: 1px solid #000; border-spacing: 0" border="1" width="100%">
    <thead>
      <tr>
        <th rowspan="2">No.</th>
        <th rowspan="2">Nama Tower</th>
        <th rowspan="2">Jenis</th>
        <th rowspan="2">Pondasi</th>
        <th colspan="3">Status Tower</th>
      </tr>
      <tr>
        <th>Status</th>
        <th>Progress</th>
        <th>Deviasi</th>
      </tr>
    </thead>
    <?php
    $no = 1;
    foreach($arrOngoing as $r) {
      $stat = '';
      if($r['StatusCode']==1) $stat='BELUM BEBAS';
      else if($r['StatusCode']==2) $stat='IDLE PONDASI';
      else if($r['StatusCode']==3) $stat='ONGOING PONDASI';
      else if($r['StatusCode']==4) $stat='MASA TUNGGU UMUR BETON';
      else if($r['StatusCode']==5) $stat='IDLE ERECTION';
      else if($r['StatusCode']==6) $stat='ONGOING ERECTION';
      else if($r['StatusCode']==7) $stat='SELESAI';
      ?>
      <tr>
        <td style="text-align: right"><?=$no?>.</td>
        <td><?=$r[COL_WORKTITLE]?></td>
        <td><?=$r[COL_WORKTYPE1]?> +<?=$r[COL_WORKEXT]?></td>
        <td>Kelas <?=$r[COL_WORKTYPE2]?></td>
        <td><?=$stat?></td>
        <td style="text-align: right"><?=$r['StatusCode']==3?$r['Progress1'].'%':($r['StatusCode']==6?$r['Progress2'].'%':'-')?></td>
        <td style="text-align: right"><?=$r['StatusCode']==3?$r['ProgressDev1'].'%':($r['StatusCode']==6?$r['ProgressDev2'].'%':'-')?></td>
      </tr>
      <?php
      $no++;
    }
    ?>
  </table>
</body>
</html>
